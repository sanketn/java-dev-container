## java-dev-container ##

This container allows the user to use it as a development environment for java projects. This have maven already configured.


#### How to use ? ####

```
docker container create --name javadev -v "$(PWD)/project:/home/project" -v "$(PWD)/maven_m2:/root/.m2"  sanketnaik/java-dev-container
docker container start javadev
```
The above command should create your containers and using the below command you can start the same. 

```
PLEASE NOTE: Prior to starting the containers, please create the respective folders: $(PWD)/project and $(PWD)/maven_m2
```

#### How to use  it with compose ? ####

```
version: '3'
volumes:
  javavol:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: $HOME/javadev
  mavenvol:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: $HOME/maven_m2

services:
  javadev:
    image: sanketnaik/java-dev-container
    volumes:
      - javavol: /home/project
      - mavenvol: /root/.m2
```
The above is a sample compose file. 

```
PLEASE NOTE: Prior to starting the containers, please create the respective folders: $HOME/project and $HOME/maven_m2
```

#### Using for Developoment with vscode ####

Once the stack/container is up, then you can follow the instructions [here](https://code.visualstudio.com/docs/remote/containers#_attaching-to-running-containers) to attach Visual Studio Code and use this container as a development environment.
