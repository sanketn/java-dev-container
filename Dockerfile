FROM openjdk:jdk-buster

ARG MAVEN_VERSION=3.6.3
ENV TINI_VERSION v0.19.0
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

LABEL maintainer="Sanket Naik <sanketn@gmail.com>"
RUN mkdir -p /home/project && \
    mkdir -p /root/.m2 && \
    mkdir -p /home/tools/maven && \
    curl -fsSL -o /tmp/apache-maven.tar.gz ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz && \
    tar -xzf /tmp/apache-maven.tar.gz -C /home/tools/maven --strip-components=1 && \
    rm -f /tmp/apache-maven.tar.gz

ENV MAVEN_HOME /home/tools/maven
ENV PATH=$PATH:/home/tools/maven/bin

ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--", "tail", "-f", "/dev/null"]
WORKDIR "/home/project"